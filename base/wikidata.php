<?php
 
// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;
 
function wikidata_declarer_champs_extras($champs = array()) {
 
    // SAISIE CHAMP WIKIDATA SUR L'OBJET ARTICLE
    $champs['spip_articles']['wikidata'] = array(
        // Le type de saisie
        'saisie' => 'input',
        'options' => array(
            // Le nom du champ dans la base de données
            'nom' => 'wikidata', 
            // Le label affiché dans l'espace privé du site
            'label' => _T('wikidata:instance_wikidata'),
            // L'explication affichée dans l'espace privé du site
            'explication' => _T('wikidata:explication_instance_wikidata'),
            // Le type d'info attendue
            'sql' => "tinytext NOT NULL DEFAULT ''",
            // La valeur par défaut
            'defaut' => '',
            // Le champ est obligatoire
            'obligatoire' => 'non',
       )
    );
 
    $champs['spip_rubriques']['wikidata'] = array(
        // Le type de saisie
        'saisie' => 'input',
        'options' => array(
            // Le nom du champ dans la base de données
            'nom' => 'wikidata', 
            // Le label affiché dans l'espace privé du site
            'label' => _T('wikidata:instance_wikidata'),
            // L'explication affichée dans l'espace privé du site
            'explication' => _T('wikidata:explication_instance_wikidata'),
            // Le type d'info attendue
            'sql' => "tinytext NOT NULL DEFAULT ''",
            // La valeur par défaut
            'defaut' => '',
            // Le champ est obligatoire
            'obligatoire' => 'non',
       )
    );
	
    return $champs;	
}