<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'autres_formes' => 'Autres formes du nom',

	
	// E
	'explication_instance_wikidata' => 'Associer un item Wikidata (ex: Q127609) à l\'objet',
	

	// I
	'instance_wikidata' => 'Item Wikidata associé',
	

	// R
	'references' => 'Références extérieures : ',
	'retrouvez_sur' => 'Retrouvez cet élément sur : ',
	

);
