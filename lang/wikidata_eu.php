<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'autres_formes' => 'Izenaren beste forma batzuk: ',

	
	// E
	'explication_instance_wikidata' => 'Wikidata itema bat objektuari lotu (adib.: Q127609)',
	

	// I
	'instance_wikidata' => 'Lotutako Wikidata itema',
	

	// R
	'references' => 'Kanpoko erreferentziak: ',
	'retrouvez_sur' => 'Ikus elementu hau hemen: ',
	

);
