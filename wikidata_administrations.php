<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
 
// Inclure l'API Champs Extras
include_spip('inc/cextras');
// Inclure les champs déclarés à l'étape précédente
include_spip('base/wikidata');
 
function wikidata_upgrade($nom_meta_base_version,$version_cible) {
 
	$maj = array();
 
	// Première déclaration à l'installation du plugin
	cextras_api_upgrade(wikidata_declarer_champs_extras(), $maj['create']);
	
	// Ajout d'un nouveau ou plusieurs champs
	cextras_api_upgrade(wikidata_declarer_champs_extras(), $maj['0.1.2']);
 
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
 
}
 
// Désinstaller proprement le plugin en supprimant les champs de la base de données
function wikidata_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(wikidata_declarer_champs_extras());
	effacer_meta($nom_meta_base_version);
}