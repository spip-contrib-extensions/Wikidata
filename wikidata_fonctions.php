<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
function wikidata_insert_head_css($flux) {
  $css = produire_fond_statique('css/wikidata.css');
  $flux .= '<link rel="stylesheet" href="'.$css.'" type="text/css" media="all" />';
  return $flux;
}

/**
[(#WIKIDATA|label{lang})] renvoit le titre utilisé par wikidata en 'lang'.
Si le label dans 'lang' est absent, ne renvoit rien.
**/
function label($q,$hizk='eu'){
	$jsonlabel = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$objlabel = json_decode($jsonlabel, true);
	if(isset($objlabel['entities'][$q]['labels'][$hizk]['value']))
		{return $lnag.$objlabel['entities'][$q]['labels'][$hizk]['value'];}
}
/**
[(#WIKIDATA|label2{lang})] renvoit le titre utilisé par wikidata en 'lang'.
Si le label dans 'lang' est absent, renvoit label en en, puis fr, peuis eu, puis es
**/
function label2($q,$hizk='eu'){
	$jsonlabel = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$objlabel = json_decode($jsonlabel, true);
	if(isset($objlabel['entities'][$q]['labels'][$hizk]['value']))
		{return $lnag.$objlabel['entities'][$q]['labels'][$hizk]['value'];}
	elseif(isset($objlabel['entities'][$q]['labels']['en']['value'])){return '<span class="wikidata absent">'.$objlabel['entities'][$q]['labels']['eu']['value'].' (en)</span>';}
	elseif(isset($objlabel['entities'][$q]['labels']['fr']['value'])){return '<span class="wikidata absent">'.$objlabel['entities'][$q]['labels']['eu']['value'].' (fr)</span>';}
	elseif(isset($objlabel['entities'][$q]['labels']['eu']['value'])){return '<span class="wikidata absent">'.$objlabel['entities'][$q]['labels']['eu']['value'].' (eu)</span>';}
	elseif(isset($objlabel['entities'][$q]['labels']['es']['value'])){return '<span class="wikidata absent">'.$objlabel['entities'][$q]['labels']['eu']['value'].' (es)</span>';}
}

/**
[(#WIKIDATA|decription{lang})] renvoit la description utilisée par wikidata en 'lang'.
Si le label dans 'lang' est absent, ne renvoit rien
**/
function description($q,$hizk='eu'){
	$jsonlabel = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$objlabel = json_decode($jsonlabel, true);
	if(isset($objlabel['entities'][$q]['descriptions'][$hizk]['value']))
		{return $objlabel['entities'][$q]['descriptions'][$hizk]['value'];}
}

/**
[(#WIKIDATA|description2{lang})] renvoit la description utilisée par wikidata en 'lang'.
Si le label dans 'lang' est absent, renvoit description en en, puis fr, puis eu, puis es
**/
function description2($q,$hizk='eu'){
	$jsonlabel = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$objlabel = json_decode($jsonlabel, true);
	if(isset($objlabel['entities'][$q]['descriptions'][$hizk]['value']))
		{return $objlabel['entities'][$q]['descriptions'][$hizk]['value'];}
	elseif(isset($objlabel['entities'][$q]['descriptions']['en']['value'])){return '<span class="wikidata absent">'.$objlabel['entities'][$q]['descriptions']['en']['value'].' (en)</span>';}
	elseif(isset($objlabel['entities'][$q]['descriptions']['fr']['value'])){return '<span class="wikidata absent">'.$objlabel['entities'][$q]['descriptions']['fr']['value'].' (fr)</span>';}
	elseif(isset($objlabel['entities'][$q]['descriptions']['eu']['value'])){return '<span class="wikidata absent">'.$objlabel['entities'][$q]['descriptions']['eu']['value'].' (eu)</span>';}
	elseif(isset($objlabel['entities'][$q]['descriptions']['es']['value'])){return '<span class="wikidata absent">'.$objlabel['entities'][$q]['descriptions']['es']['value'].' (es)</span>';}
}

/**
[(#WIKIDATA|wikipedia{lang})] renvoit le lien vers l'article wikipedia en 'lang' correspondant à l'item.
Si absent, ne renvoit rien
[(#WIKIDATA|wikipedialink{lang})] renvoit l'url de l'article wikipedia
[(#WIKIDATA|wikipedia{lang})] renvoit le titre de l'article wikipedia
**/
function wikipedia($q,$hizk='eu'){
	$jsonlabel = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$objlabel = json_decode($jsonlabel, true);
	if(isset($objlabel['entities'][$q]['sitelinks'][$hizk.'wiki']))
		{return '<a href="https://'.$hizk.'.wikipedia.org/wiki/'.$objlabel['entities'][$q]['sitelinks'][$hizk.'wiki']['title'].'">'.$objlabel['entities'][$q]['sitelinks'][$hizk.'wiki']['title'].'</a>';}
}
function wikipedialink($q,$hizk='eu'){
	$jsonlabel = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$objlabel = json_decode($jsonlabel, true);
	if(isset($objlabel['entities'][$q]['sitelinks'][$hizk.'wiki']))
		{return 'https://'.$hizk.'.wikipedia.org/wiki/'.$objlabel['entities'][$q]['sitelinks'][$hizk.'wiki']['title'];}
}
function wikipediatitle($q,$hizk='eu'){
	$jsonlabel = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$objlabel = json_decode($jsonlabel, true);
	if(isset($objlabel['entities'][$q]['sitelinks'][$hizk.'wiki']))
		{return $objlabel['entities'][$q]['sitelinks'][$hizk.'wiki']['title'];}
}
/**
[(#WIKIDATA|wikilink)] renvoit le lien vers wikidata
ex : [<a href="(#WIKIDATA|wikilink)" alt=lien wikidata">Voir sur Wikidata</a>]
**/
function wikilink($q){
	return 'https://www.wikidata.org/wiki/'.$q;
}

/**
[(#WIKIDATA|property{property,lang})] renvoit le contenu du champs 'property' de l'item wikidata.
Renvoit un string s'il n'y a qu'un seul résultat, une liste séparé par de virgules sinon.

- S'il s'agit d'un item Wikipedia, renvoit le label 'lang' de l'item
ex : [(#WIKIDATA|property{P31,fr})] => 'être humain'
ex : [(#WIKIDATA|property{P21,en})] => 'male'
ex : [(#WIKIDATA|property{P27,eu})] => 'Frantzia'

- S'il s'agit d'un string, renvoit le string (pas besoin de langue)
ex : pseudonyme [(#WIKIDATA|property{P742})]

- S'il s'agit d'un External-Id (string), renvoit le string (pas besoin de langue)
ex : identifiant viaf [(#WIKIDATA|property{P214})]
ou
Isni  : [<a href="[https://isni.org/isni/(#WIKIDATA|property{P213}|replace{' '})]">(#WIKIDATA|property{P213})</a>]
Viaf  : [<a href="[https://viaf.org/viaf/(#WIKIDATA|property{P214})]">(#WIKIDATA|property{P214})</a>]
D-nb : [<a href="[https://d-nb.info/gnd/(#WIKIDATA|property{P227})]">(#WIKIDATA|property{P227})</a>]
LOC : [<a href="[https://id.loc.gov/authorities/(#WIKIDATA|property{P244})]">(#WIKIDATA|property{P244})</a>]
BnF : [<a href="[https://catalogue.bnf.fr/ark:/12148/cb(#WIKIDATA|property{P268})]">(#WIKIDATA|property{P268})</a>]
IdRef : [<a href="[https://www.idref.fr/(#WIKIDATA|property{P269})]">(#WIKIDATA|property{P269})</a>]
BNE : [<a href="[https://datos.bne.es/persona/(#WIKIDATA|property{P950})]">(#WIKIDATA|property{P950})</a>]
Twitter : [<a href="[https://twitter.com/(#WIKIDATA|property{P2002})]">(#WIKIDATA|property{P2002})</a>]
Instagram : [<a href="[https://www.instagram.com/(#WIKIDATA|property{P2003})]">(#WIKIDATA|property{P2003})</a>]
FAST : [<a href="[http://id.worldcat.org/fast/(#WIKIDATA|property{P2163})]">(#WIKIDATA|property{P2163})</a>]
Google Knowledge : [<a href="[https://www.google.com/search?kgmid=(#WIKIDATA|property{P2671})]">(#WIKIDATA|property{P2671})</a>]
Auñamendi : [<a href="[http://www.euskomedia.org/aunamendi/(#WIKIDATA|property{P3218})]">(#WIKIDATA|property{P3218})</a>]
Lit zubitegia : [<a href="[https://zubitegia.armiarma.eus/?i=(#WIKIDATA|property{P5985})]">(#WIKIDATA|property{P5985})</a>]
Europeana : [<a href="[https://data.europeana.eu/agent/base/(#WIKIDATA|property{P7704})]">(#WIKIDATA|property{P7704})</a>]
WorldCat : [<a href="[https://www.worldcat.org/identities/(#WIKIDATA|property{P7859})]">(#WIKIDATA|property{P7859})</a>]
Lur Hiztegia : [<a href="[https://www.euskadi.eus/web01-a2lurhiz/eu/contenidos/termino/_c(#WIKIDATA|property{P10242}).html]">(#WIKIDATA|property{P10242})</a>]


- S'il s'agit d'un time, renvoit la date au format date  (pas besoin de langue)
date de naissance [(#WIKIDATA|property{P569})]
année de naissance [(#WIKIDATA|property{P569}|annee)]
date de décès[(#WIKIDATA|property{P570})]
année de décès [(#WIKIDATA|property{P570}|annee)]


- S'il s'agit d'une entrée de type Monolingualtext, renvoit la liste des entrées sous format 'string (lang)'
Attention, passer un paramètre lange ne change rien.
Ex : Nom officiel [(#WIKIDATA|property{P1448})]

- S'il s'agit d'une entrée de type Globe-ccordinate, renvoit les coordonnées de geolocalisation
Ex : Coordonnées gégraphiques :  [(#WIKIDATA|property{P625})] -> 43.4925, -1.4763888888889

- S'il s'agit d'un fichier commons media, renvoit l'url de l'image
Ex : Image : [(#WIKIDATA|property{P18}|balise_img{image issue de wikidata,wikidataImg,150})]

**/
function property($q,$property='P31',$hizk='eu'){
	$json = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$obj = json_decode($json, true);
	// wikibase-item
	if(isset($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value']['entity-type']) 
		&& ($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value']['entity-type']=='item'))
		{$count=count($obj['entities'][$q]['claims'][$property]);
		if ($count>1){
			$return='';
			for($i=0;$i<$count;$i++) {
				$return.=label($obj['entities'][$q]['claims'][$property][$i]['mainsnak']['datavalue']['value']['id'],$hizk);
				if($i<($count-1)){$return.=', ';}
				}
			return $return;
			}
		else {return label($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value']['id'],$hizk);}
		}
	// String
	elseif(isset($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']=='string'))
		{$count=count($obj['entities'][$q]['claims'][$property]);
		if ($count>1){
			$return='';
			for($i=0;$i<$count;$i++) {
				$return.=$obj['entities'][$q]['claims'][$property][$i]['mainsnak']['datavalue']['value'];
				if($i<($count-1)){$return.=', ';}
				}
			return $return;
			}
		else {return $obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value'];}
		}
	//External-id (string)
	elseif(isset($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']=='external-id'))
		{$count=count($obj['entities'][$q]['claims'][$property]);
		if ($count>1){
			$return='';
			for($i=0;$i<$count;$i++) {
				$return.=$obj['entities'][$q]['claims'][$property][$i]['mainsnak']['datavalue']['value'];
			if($i<($count-1)){$return.=', ';}
				}
			return $return;
			}
		else {return $obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value'];}	
		}
	//time
	elseif(isset($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']=='time'))
		{$count=count($obj['entities'][$q]['claims'][$property]);
		if ($count>1){
			$return='';
			for($i=0;$i<$count;$i++) {
			$hdate = new DateTime($obj['entities'][$q]['claims'][$property][$i]['mainsnak']['datavalue']['value']['time']);
			//$return.=$hdate->format('Y-m-d H:i:s');
			$return=$hdate;
			if($i<($count-1)){$return.=', ';}
				}
			return $return;
			}
		else {$hdate = new DateTime($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value']['time']);
			return $hdate->format('Y-m-d H:i:s');}
		}
	// Monolingualtext
	elseif(isset($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']=='monolingualtext'))
		{$count=count($obj['entities'][$q]['claims'][$property]);
		if ($count>1){$return='<ul>';for($i=0;$i<$count;$i++) {
			$return.='<li>'.$obj['entities'][$q]['claims'][$property][$i]['mainsnak']['datavalue']['value']['text'].' ('.$obj['entities'][$q]['claims'][$property][$i]['mainsnak']['datavalue']['value']['language'].')</li>';}
			$return.='</ul>';return $return;		
		}
		else {return $obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value']['text'].' ('.$obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value']['language'].')';}
		}
	// globe-coordinate
	elseif(isset($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']=='globe-coordinate'))
		{$count=count($obj['entities'][$q]['claims'][$property]);
		if ($count>1){$return='<ul>';for($i=0;$i<$count;$i++) {
			$return.='<li>'.$obj['entities'][$q]['claims'][$property][$i]['mainsnak']['datavalue']['value']['latitude'].', '.$obj['entities'][$q]['claims'][$property][$i]['mainsnak']['datavalue']['value']['longitude'].'</li>';}
			$return.='</ul>';return $return;		
		}
		else {return $obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value']['latitude'].', '.$obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value']['longitude'];}
		}

	//commonsMedia
	elseif(isset($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims'][$property][0]['mainsnak']['datatype']=='commonsMedia'))
		{
			$img = str_replace(' ','_',$obj['entities'][$q]['claims'][$property][0]['mainsnak']['datavalue']['value']);
			$img = str_replace('É','\u00c9',$img);
			$json2 = file_get_contents('https://commons.wikimedia.org/w/api.php?action=query&titles=File:'.$img.'&prop=imageinfo&&iiprop=url&iiurlwidth=300&format=json');
			$obj2 = json_decode($json2, true);
			foreach( $obj2['query']['pages'] AS $key => $val ){
				$img2= $obj2['query']['pages'][$key]['imageinfo'][0]['thumburl'];
			return $img2;
			}
		}
}
/**
[(#WIKIDATA|alias{lang})] renvoit le les termes passés en label et en alias dans wikipedia dans cette langue
Si absent, ne renvoit rien
**/
function alias($q,$hizk='eu'){
	$array_alias = array();
	$json = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$obj = json_decode($json, true);
	if (isset($obj['entities'][$q]['labels'][$hizk]['value']))
		{$array_alias[] = $obj['entities'][$q]['labels'][$hizk]['value'];}
	for($i=0;$i<10;$i++)
		{if(strlen($obj['entities'][$q]['aliases'][$hizk][$i]['value'])>1)
			{$array_alias[] = $obj['entities'][$q]['aliases'][$hizk][$i]['value'];}
		}
	$garbi = array_unique($array_alias);
	$alias=  '';
	foreach($garbi as $val) {$alias .=$val.'; ';}
	return $alias;
}
/**
[(#WIKIDATA|alias2{lang})] renvoit le les termes passés en alias dans wikipedia dans cette langue + dans une dizaine d'autres langues
Si absent, ne renvoit rien
**/
function alias2($q,$hizk='eu'){
	$array_alias = array();
	$langarray = array("eu","fr","es","en","oc","br","oc","de","ca","la");	
	$langarray[] = $hizk;
	$langarray = array_unique($langarray);
	$json = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$obj = json_decode($json, true);
	foreach ($langarray as $key => $lg) {
		if (isset($obj['entities'][$q]['labels'][$lg]['value']))
			{$array_alias[] = $obj['entities'][$q]['labels'][$lg]['value'];}
		if (isset($obj['entities'][$q]['labels'][$lg]))
			{$count=count($obj['entities'][$q]['labels'][$lg]);
			for($i=0;$i<$count;$i++)
				{if(strlen($obj['entities'][$q]['aliases'][$lg][$i]['value'])>1)
					{$array_alias[] = $obj['entities'][$q]['aliases'][$lg][$i]['value'];}
				}
			}
	}
	if(isset($obj['entities'][$q]['claims']['P1448'][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims']['P1448'][0]['mainsnak']['datatype']=='monolingualtext'))
		{$count=count($obj['entities'][$q]['claims']['P1448']);
		if ($count>1){
			for($i=0;$i<$count;$i++) {
			$array_alias[] = $obj['entities'][$q]['claims']['P1448'][$i]['mainsnak']['datavalue']['value']['text'];}	
		}
		else {$array_alias[] = $obj['entities'][$q]['claims']['P1448'][0]['mainsnak']['datavalue']['value']['text'];}
		}
		
	$garbi = array_unique($array_alias);
	$alias=  '';
	foreach($garbi as $val) {$alias .=$val.'; ';}
	return $alias;
		
}


/**
Quelques fonctions supplémentaires
**/

/**
[(#WIKIDATA|latitude)] Renvoit la latitude si l'entité dispose d'une propriété P625. Renvoit le premier résultats par défaut. Le paramètre $nb renvoit une liste de latititude s'il y en a plusieurs
ex : Latitude (1 entrée max) : [(#WIKIDATA|latitude)]
ex : Latitude (2 entrées max) : [(#WIKIDATA|latitude{2})]
**/

function latitude($q,$nb=1) {
	$json = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$obj = json_decode($json, true);
	if(isset($obj['entities'][$q]['claims']['P625'][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims']['P625'][0]['mainsnak']['datatype']=='globe-coordinate'))
		{$count=count($obj['entities'][$q]['claims']['P625']);
		if($count>$nb){$count=$nb;}
		if ($count>1){$return='<ul>';for($i=0;$i<$count;$i++) {
			$return.='<li>'.$obj['entities'][$q]['claims']['P625'][$i]['mainsnak']['datavalue']['value']['latitude'].'</li>';}
			$return.='</ul>';return $return;		
		}
		else {return $obj['entities'][$q]['claims']['P625'][0]['mainsnak']['datavalue']['value']['latitude'];}
		}
}
/**
[(#WIKIDATA|longitude)] Renvoit la longitude si l'entité dispose d'une propriété P625. Renvoit le premier résultats par défaut. Le paramètre $nb renvoit une liste de latititude s'il y en a plusieurs
ex : Longitude (1 entrée max) : [(#WIKIDATA|longitude)]
ex : Longitude (2 entrées max) : [(#WIKIDATA|longitude{2})]
**/

function longitude($q,$nb=1) {
	$json = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$obj = json_decode($json, true);
	if(isset($obj['entities'][$q]['claims']['P625'][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims']['P625'][0]['mainsnak']['datatype']=='globe-coordinate'))
		{$count=count($obj['entities'][$q]['claims']['P625']);
		if($count>$nb){$count=$nb;}
		if ($count>1){$return='<ul>';for($i=0;$i<$count;$i++) {
			$return.='<li>'.$obj['entities'][$q]['claims']['P625'][$i]['mainsnak']['datavalue']['value']['longitude'].'</li>';}
			$return.='</ul>';return $return;		
		}
		else {return $obj['entities'][$q]['claims']['P625'][0]['mainsnak']['datavalue']['value']['longitude'];}
		}
}
/**
[(#WIKIDATA|naissance)] renvoit l'année de naissance (P569) de l'item s'il existe. Ne renvoit rien sinon
**/
function naissance($q){
	$json = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$obj = json_decode($json, true);
	if(isset($obj['entities'][$q]['claims']['P569'][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims']['P569'][0]['mainsnak']['datatype']=='time'))
		{$count=count($obj['entities'][$q]['claims']['P569']);
		if ($count>1){
			$return='';
			for($i=0;$i<$count;$i++) {
			$hdate = new DateTime($obj['entities'][$q]['claims']['P569'][$i]['mainsnak']['datavalue']['value']['time']);
			$return.=$hdate->format('Y');
			if($i<($count-1)){$return.=', ';}
				}
			return $return;
			}
		else {$hdate = new DateTime($obj['entities'][$q]['claims']['P569'][0]['mainsnak']['datavalue']['value']['time']);
			return $hdate->format('Y');}
		}
}
/**
[(#WIKIDATA|deces)] renvoit l'année de deces (P570) de l'item s'il existe. Ne renvoit rien sinon
**/
function deces($q){
	$json = file_get_contents("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json");
	$obj = json_decode($json, true);
	if(isset($obj['entities'][$q]['claims']['P570'][0]['mainsnak']['datatype']) 
		&& ($obj['entities'][$q]['claims']['P570'][0]['mainsnak']['datatype']=='time'))
		{$count=count($obj['entities'][$q]['claims']['P570']);
		if ($count>1){
			$return='';
			for($i=0;$i<$count;$i++) {
			$hdate = new DateTime($obj['entities'][$q]['claims']['P570'][$i]['mainsnak']['datavalue']['value']['time']);
			$return.=$hdate->format('Y');
			if($i<($count-1)){$return.=', ';}
				}
			return $return;
			}
		else {$hdate = new DateTime($obj['entities'][$q]['claims']['P570'][0]['mainsnak']['datavalue']['value']['time']);
			return $hdate->format('Y');}
		}
}